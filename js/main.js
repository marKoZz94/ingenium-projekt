$(document).ready(function(){

  // Tabbed Content

  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });


  // Menu initialization

  var $menuIcon = $('.menu-icon'),
      $navigation = $('.navigation'),
      $mainNavigation = $('.main-navigation'),
      $navigationLink = $('.main-navigation a');

  $(window).scroll(function() {
    if(window.scrollY > window.outerHeight) {
      $menuIcon.addClass('active');
    } else {
      $menuIcon.removeClass('active');
    }
    /*Scroll to top when arrow up clicked BEGIN*/
    var height = $(window).scrollTop();
      if (height > 100) {
          $('#back2Top').fadeIn();
      } else {
          $('#back2Top').fadeOut();
      }
  });

  $menuIcon.click(function(e) {
    e.preventDefault();

    $navigation.toggleClass('active');
  });

  /*Scroll to top when arrow up clicked END*/
  $("#back2Top").click(function(event) {
          event.preventDefault();
          $("html, body").animate({ scrollTop: 0 }, "slow");
          return false;
      });

 // Scroll fix
    $('a[href^="#"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if( target.length ) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 1000)
      $navigation.removeClass('active');;
        }
    });


  // Scrollreveal initialize
  var config = {

    easing: 'hustle',
    reset:  false,
    delay:  'onload',
    opacity: .2,
    vFactor: 0.2,
    mobile: false
  }

  window.sr = new scrollReveal( config );

});

/* Map */
function initMap() {
        var uluru = {lat: 45.5447842, lng: 19.7986772};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }

function dropDown(li) {
    var submenu = li.getElementsByTagName('ul')[0];
    if( submenu) {
        submenu.style.display = submenu.style.display == "block" ? "" : "block";
    }
}
